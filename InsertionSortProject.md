[22,27,16,2,18,6] -> Insertion Sort

**Q**:Yukarı verilen dizinin sort türüne göre aşamalarını yazınız.
**A**: 

    1.  [16 22 27 2 18 6]
    2.  [2 16 22 27 18 6]
    3.  [2 16 18 22 27 6]
    4.  [2 6 16 18 22 27]

**Q**: Big-O gösterimini yazınız.
**A**: O(n^2)

Time Complexity: Dizi sıralandıktan sonra 18 sayısı aşağıdaki case'lerden hangisinin kapsamına girer? Yazınız
**A**: Average case: Aradığımız sayının ortada olması
