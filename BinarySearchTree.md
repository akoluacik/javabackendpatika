[7, 5, 1, 8, 3, 6, 0, 9, 4, 2] dizisinin Binary-Search-Tree aşamalarını yazınız.

Root = 7.


            7
           / \
          5   8
         / \   \
        1   6   9
       /  \
      0    3
          / \
         2   4


- 5 < 7. Bu yüzden sol tarafta.
- 1 < 7 ve 1 < 5. Bu yüzden 1 5'in solunda olmalı.

- 8 > 7, o yüzden 7'nin sağında olmalı.
- 3 < 7 ve 3 < 5 fakat 3 > 1. Bu yüzden 3 7 ve 5'in solunda fakat 1'in sağında olmalı.
- 6 < 7 fakat 6 > 5. Bu yüzden 6 7'nin solunda ve 5'in sağında olmalı.
- 0 < 7 ve 0 < 5 ve 0 < 1 olduğu için 0 tüm bu sayıların solunda olmalı.
- 9 > 7 ve 9 > 8 olduğu için 9 bu sayıların sağında olmalı.
- 4 < 7 ve 4 < 5 fakat 4 > 1 ve 4 > 3. Bu yüzden 4, 7 ve 5'in solunda fakat 1 ve 3'ün sağında olmalı.
- 2 < 7 ve 2 < 5 ve 2 < 3 olduğu için 2 bu sayıların solunda olmalı.
